// Copyright: (c) 2023, Michele Valsecchi <https://gitlab.com/micheleva>
// GNU General Public License v3.0+
// (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

// Package root contains the entry point for the command inititalization logic
package root

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/micheleva/ali/cmd"
	"gitlab.com/micheleva/ali/io"
)

func NewCmdRoot() *cobra.Command {

	var rootCmd = &cobra.Command{
		Use:     "ali",
		Short:   "An application to track daily nutrition intake",
		Version: fmt.Sprintf("%.1f", cmd.Version),
	}

	alimentFinderDatabaseWriter := cmd.IoAlimentFinderDatabaseWriter{}
	addCmd := cmd.NewCmdAdd(alimentFinderDatabaseWriter)
	rootCmd.AddCommand(addCmd)

	alimentsCmd := cmd.NewCmdAliments(cmd.ListAlimentsDb)
	dbHelper := io.DefaultDatabaseHelper{}
	listCmd := cmd.NewCmdList(io.ReadAlimentArrayFromFile, dbHelper)
	listCmd.AddCommand(alimentsCmd)
	rootCmd.AddCommand(listCmd)

	alimentFindAdder := cmd.IoAlimentFinder{}
	newCmd := cmd.NewCmdNew(alimentFindAdder)
	rootCmd.AddCommand(newCmd)

	versionCmd := cmd.NewCmdVersion()
	rootCmd.AddCommand(versionCmd)
	return rootCmd
}
