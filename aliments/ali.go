// Copyright: (c) 2023, Michele Valsecchi <https://gitlab.com/micheleva>
// GNU General Public License v3.0+
// (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

// Package aliments package exports struct(s)
package aliments

// Aliment struct represents foods or drinks
type Aliment struct {
	Name     string  `json:"name"`
	Calories float64 `json:"calories"`
	Proteins float64 `json:"proteins"`
}
