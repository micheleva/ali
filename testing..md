# Run test for all subdirs
go test ./...

# Display test coverage for all subdirs
go test -cover ./...

# Prepare coverage data for all subdirs
go test -coverprofile=coverage.out ./...
go tool cover -func=coverage.out

# View in html
go tool cover -html=coverage.out
