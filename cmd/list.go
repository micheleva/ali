package cmd

import (
	"errors"
	"fmt"
	"os"
	"time"

	"github.com/spf13/cobra"
	a "gitlab.com/micheleva/ali/aliments"
	"gitlab.com/micheleva/ali/io"
)

// See https://github.com/kubernetes/kubectl/blob/master/pkg/cmd/create/create_secret.go#L90-L118
type ListOptions struct {
	date string
}

func NewCmdList(f func(a string) ([]a.Aliment, error), dbh io.DatabaseHelper) *cobra.Command {
	// See https://github.com/kubernetes/kubectl/blob/master/pkg/cmd/create/create_secret.go#L121-L130
	o := &ListOptions{}
	newCmd := &cobra.Command{
		Use:   "list",
		Short: "List entries for a given day",
		Long: `
List entries for a given day [...]
`,
		Args: func(cmd *cobra.Command, args []string) error {
			if err := cobra.NoArgs(cmd, args); err != nil {
				return err
			}
			// Input validation
			if _, err := time.Parse("2006-01-02", o.date); err != nil {
				return errors.New("Please enter a valid date. Aborting.")
			}

			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			journalsRoot, err := dbh.GetCorrectPath("journalsRoot")
			if err != nil {
				return err
			}
			arrayOfAliments, err := f(fmt.Sprintf(journalsRoot+"/%v/data.json", o.date))
			if err == nil {
				if len(arrayOfAliments) == 0 {
					return errors.New(fmt.Sprintf("The database has no entry for %v.", o.date))
				} else {
					var totalProteins, totalCalories float64 = 0.0, 0.0
					for _, loggedAl := range arrayOfAliments {
						fmt.Println(loggedAl)
						totalProteins += loggedAl.Proteins
						totalCalories += loggedAl.Calories
					}
					fmt.Printf("Total Calories: %.2fKcal, Total proteins %.2fgr for %v.\n", totalCalories, totalProteins, o.date)
					return nil
				}
			} else {
				if errors.Is(err, os.ErrNotExist) {
					return errors.New(fmt.Sprintf("The database has no entry for %v.\n", o.date))
				} else {
					return errors.New(fmt.Sprintf("It seems we have problems retrieving data for %v\n", o.date))
				}
			}
			return nil
		},
	}
	newCmd.Flags().StringVarP(&o.date, "date", "d", time.Now().Format("2006-01-02"), "Date to display aliment intake for")
	return newCmd
}
