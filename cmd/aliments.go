package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/micheleva/ali/io"
)

// TODO stop using this, and use ListAlimentsDbFromPath instead
func ListAlimentsDb() error {
	dbFullPath, err := io.GetCorrectPath("dbFullPath")
	if err != nil {
		return err
	}
	arrayOfAliments, err := io.ReadAlimentArrayFromFile(dbFullPath)
	if err != nil {
		return err
	}
	if len(arrayOfAliments) == 0 {
		fmt.Println("The database is empty.")
	} else {
		for i, dbEntry := range arrayOfAliments {
			fmt.Printf("Index %v: %v\n", i, dbEntry)
		}
	}
	return nil
}

func ListAlimentsDbFromPath(path string) error {
	arrayOfAliments, err := io.ReadAlimentArrayFromFile(path)
	if err != nil {
		return err
	}
	if len(arrayOfAliments) == 0 {
		fmt.Println("The database is empty.")
	} else {
		for i, dbEntry := range arrayOfAliments {
			fmt.Printf("Index %v: %v\n", i, dbEntry)
		}
	}
	return nil
}

func NewCmdAliments(f func() error) *cobra.Command {
	// TODO: inject an argument of type DBtype, so that we can mock it later in the tests => NewCmdAliments(dbCon &io.DbConnector)
	return &cobra.Command{
		Use:   "aliments",
		Short: "List aliments in the database",
		Args:  cobra.NoArgs,
		RunE: func(cmd *cobra.Command, args []string) error {
			if err := f(); err != nil {
				return err
			}
			return nil
		},
	}
}
