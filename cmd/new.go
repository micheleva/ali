package cmd

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	a "gitlab.com/micheleva/ali/aliments"
	"gitlab.com/micheleva/ali/io"
)

type AlimentFindAdder interface {
	FindAlimentByName(string) (a.Aliment, error)
	AddAlimentToDatabase(a.Aliment) error
}

type IoAlimentFinder struct{}

func (r IoAlimentFinder) FindAlimentByName(aliName string) (a.Aliment, error) {
	return io.FindAlimentByName(aliName)
}
func (r IoAlimentFinder) AddAlimentToDatabase(aliObj a.Aliment) error {
	return (io.AddAlimentToDatabase(aliObj))
}

// See https://github.com/kubernetes/kubectl/blob/6dde41fea68d64d96fd980a8dc918087bd8f89be/pkg/cmd/create/create_secret.go#L90-L118
type NewOptions struct {
	newName     string
	newCalories float64
	newProteins float64
}

func NewCmdNew(afa AlimentFindAdder) *cobra.Command {
	// See https://github.com/kubernetes/kubectl/blob/6dde41fea68d64d96fd980a8dc918087bd8f89be/pkg/cmd/create/create_secret.go#L129-L159
	o := &NewOptions{}

	newCmd := &cobra.Command{
		Use:   "new",
		Short: "Create a new entry into the database",
		Long: `
Create a new entry into the database.
Later, when adding journal entries (cobra-ali new <...>), database entries can be referenced
to speed up the logging process.
`,
		Args: func(cmd *cobra.Command, args []string) error {
			if err := cobra.NoArgs(cmd, args); err != nil {
				return err
			}

			// NOTE: allow exactly zero calories or zero proteins in order to track Water or similar entities
			if o.newCalories < 0 || o.newProteins < 0 {
				return errors.New("Calories and Proteins amount should be at least 0.0, or higher. Aborting.")
			}
			return nil
		},

		RunE: func(cmd *cobra.Command, args []string) error {
			notFound := &io.NotFoundError{}
			alimentFromCli := a.Aliment{Name: o.newName, Calories: o.newCalories, Proteins: o.newProteins}
			// Prevent duplicates
			found, err := afa.FindAlimentByName(o.newName)
			if found.Name != "" {
				return errors.New("We already have this item in the database. Can't save!")
			}
			// "Not found" error is expected: it means we're actually creating a new entry
			// Only check for other error types
			if err != nil && !errors.As(err, notFound) {
				return err
			}

			if err := afa.AddAlimentToDatabase(alimentFromCli); err != nil {
				return err
			}
			fmt.Printf("Added %v to the database!\n", alimentFromCli)
			return nil
		},
	}
	newCmd.Flags().StringVarP(&o.newName, "name", "n", o.newName, "Name for this aliment entry")
	newCmd.Flags().Float64VarP(&o.newCalories, "calories", "c", o.newCalories, "Amount of calories, in Kcal, for this entry")
	newCmd.Flags().Float64VarP(&o.newProteins, "proteins", "p", o.newProteins, "Amount of proteins, in grams, for this entry")
	newCmd.MarkFlagsRequiredTogether("calories", "proteins")
	newCmd.MarkFlagRequired("name")
	newCmd.MarkFlagRequired("calories")
	newCmd.MarkFlagRequired("proteins")
	return newCmd
}
