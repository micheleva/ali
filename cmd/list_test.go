package cmd

import (
	"bytes"
	"errors"
	"fmt"
	"os"
	"strings"
	"testing"
	"time"

	"gitlab.com/micheleva/ali/io"
)

type DbHelper interface {
	DbInit() error
	CleanDb() error
	GetCorrectPath(path string) (string, error)
}

type MockDbHelper struct {
	dbRoot       string
	journalsRoot string
	alimentsRoot string
	dbFullPath   string
}

func (dbi *MockDbHelper) CleanDb() error {
	return os.RemoveAll(dbi.dbRoot)
}

func (dbi *MockDbHelper) DbInit() error {

	// NICE TO HAVE: add some random element into the directory name to allow parallel testing
	// Also, after the random is generated, confirm there are no collisions before proceding further
	dbi.dbRoot = fmt.Sprintf("%v/%v", os.TempDir(), "ali-tests")
	dbi.journalsRoot = fmt.Sprintf("%v/journals", dbi.dbRoot)
	dbi.alimentsRoot = fmt.Sprintf("%v/aliments", dbi.dbRoot)
	dbi.dbFullPath = fmt.Sprintf("%v/aliments/db.json", dbi.dbRoot)

	if err := os.Mkdir(dbi.dbRoot, os.ModePerm); err != nil && !errors.Is(err, os.ErrExist) {
		fmt.Printf("error %v \n", err)
		return err
	}
	if err := os.Mkdir(dbi.journalsRoot, os.ModePerm); err != nil && !errors.Is(err, os.ErrExist) {
		fmt.Printf("error %v \n", err)
		return err
	}
	if err := os.Mkdir(dbi.alimentsRoot, os.ModePerm); err != nil && !errors.Is(err, os.ErrExist) {
		fmt.Printf("error %v \n", err)
		return err
	}
	f, err := os.Create(dbi.dbFullPath)
	defer f.Close()
	if err != nil {
		fmt.Printf("error %v \n", err)
		return err
	}
	// Initialize the file with an empty array to avoid JSON syntax errors when reading this empty file
	if _, err := f.WriteString("[]"); err != nil {
		fmt.Printf("error %v \n", err)
		return err
	}
	return nil
}

func (dbi MockDbHelper) GetCorrectPath(targetPath string) (string, error) {
	var mockPath string = ""
	switch targetPath {
	case "dbRoot":
		mockPath = dbi.dbRoot
	case "journalsRoot":
		mockPath = dbi.journalsRoot
	case "alimentsRoot":
		mockPath = dbi.alimentsRoot
	case "dbFullPath":
		mockPath = dbi.dbFullPath
	default:
		return "", errors.New("Unknown key: the database structure has no such path!")

	}
	return mockPath, nil
}

func Test_ListCommand_ReadDb(t *testing.T) {
	out := new(bytes.Buffer)
	err := new(bytes.Buffer)
	args := []string{}

	mockDBInitializer := MockDbHelper{}
	listCmd := NewCmdList(io.ReadAlimentArrayFromFile, mockDBInitializer)
	listCmd.SetOut(out)
	listCmd.SetErr(err)
	listCmd.SetArgs(args)

	// Inititalize the test database, and clean it up on return
	mockDBInitializer.DbInit()
	defer mockDBInitializer.CleanDb()

	listCmd.Execute()

	// Confirm we do get an error, as the DB is empty
	got := strings.TrimSpace(err.String())
	want := fmt.Sprintf("Error: The database has no entry for %v.", time.Now().Format("2006-01-02"))
	if want != got {
		t.Fatalf("\nGot%v\nWanted%v\n", got, want)
	}
}

// Correctly say there is not data for today when the db is missing entirely
func Test_ListCommand_NoDb(t *testing.T) {
	out := new(bytes.Buffer)
	err := new(bytes.Buffer)
	args := []string{}

	listCmd := NewCmdList(io.ReadAlimentArrayFromFile, MockDbHelper{})
	listCmd.SetOut(out)
	listCmd.SetErr(err)
	listCmd.SetArgs(args)

	listCmd.Execute()

	// Confirm the usage text is printed out correctly
	want := strings.TrimSpace(listCmd.UsageString())
	got := strings.TrimSpace(out.String())
	if want != got {
		t.Fatalf("\nGot%v\nWanted%v\n", got, want)
	}

	got = strings.TrimSpace(err.String())
	want = fmt.Sprintf("Error: The database has no entry for %v.", time.Now().Format("2006-01-02"))
	if want != got {
		t.Fatalf("\nGot%v\nWanted%v\n", got, want)
	}
}
