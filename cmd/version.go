package cmd

import (
	"github.com/spf13/cobra"
)

const Version float64 = 0.2

func NewCmdVersion() *cobra.Command {
	var versionCmd = &cobra.Command{
		Use:   "version",
		Short: "Display the version of the client",
		Args:  cobra.NoArgs,
		RunE: func(cmd *cobra.Command, args []string) error {
			cmd.Printf("ali version %.1f\n", Version)
			return nil
		},
	}
	return versionCmd
}
