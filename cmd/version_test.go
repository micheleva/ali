package cmd

import (
	"bytes"
	"strings"
	"testing"
)

func Test_VersionCommand(t *testing.T) {
	out := new(bytes.Buffer)
	err := new(bytes.Buffer)
	args := []string{}

	versionCmd := NewCmdVersion()

	versionCmd.SetOut(out)
	versionCmd.SetErr(err)
	versionCmd.SetArgs(args)

	versionCmd.Execute()

	want := strings.TrimSpace("ali version 0.2")
	got := strings.TrimSpace(out.String())
	if want != got {
		t.Fatalf("\nGot\n%v\nWanted\n%v\n", got, want)
	}

}
