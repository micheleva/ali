package cmd

import (
	"errors"
	"fmt"
	"time"

	"github.com/spf13/cobra"
	a "gitlab.com/micheleva/ali/aliments"
	"gitlab.com/micheleva/ali/io"
)

type AlimentFinderDatabaseWriter interface {
	AddAlimentToJournal(a.Aliment, string) error
	FindAlimentByName(string) (a.Aliment, error)
	AddAlimentToDatabase(a.Aliment) error
}

type IoAlimentFinderDatabaseWriter struct{}

func (r IoAlimentFinderDatabaseWriter) AddAlimentToJournal(aliment a.Aliment, date string) error {
	return io.AddAlimentToJournal(aliment, date)
}

func (r IoAlimentFinderDatabaseWriter) FindAlimentByName(aliment string) (a.Aliment, error) {
	return io.FindAlimentByName(aliment)
}

func (r IoAlimentFinderDatabaseWriter) AddAlimentToDatabase(aliment a.Aliment) error {
	return io.AddAlimentToDatabase(aliment)
}

// See https://github.com/kubernetes/kubectl/blob/6dde41fea68d64d96fd980a8dc918087bd8f89be/pkg/cmd/create/create_secret.go#L90-L118
type AddOptions struct {
	addDate string
	addName string
	save    bool
	raw     bool
	addCal  float64
	addProt float64
}

func NewCmdAdd(afdw AlimentFinderDatabaseWriter) *cobra.Command {
	// See https://github.com/kubernetes/kubectl/blob/6dde41fea68d64d96fd980a8dc918087bd8f89be/pkg/cmd/create/create_secret.go#L129-L159
	o := &AddOptions{}

	var addCmd = &cobra.Command{
		Use:   "add",
		Short: "Add a new entry to the journal",
		Long: `
Add a new entry to the journal

Using --name (-n) will search for that aliment in the database,
and insert into --date (-d, default today) journal.

Using --raw (-r) will allow to insert entries into --date (-d, default today) journal.
Passing --save (-s) together with --raw (-r) will also save the entry to the database
other than just adding it into --date (-d, default today) journal.
`,
		Args: func(cmd *cobra.Command, args []string) error {
			if err := cobra.NoArgs(cmd, args); err != nil {
				return err
			}

			// Input validation
			if _, err := time.Parse("2006-01-02", o.addDate); err != nil {
				return errors.New("Please enter a valid date. Aborting.")
			}

			// NOTE: allow exactly zero calories or zero proteins in order to track Water or similar entities
			if o.addCal < 0 || o.addProt < 0 {
				return errors.New("Calories and Proteins amount should be at least 0.0, or higher. Aborting.")
			}

			// "Calories,proteins and raw flags" are marked as a set, and init() is already enforcing we have all or none
			// No need to check again here
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			if !o.raw {
				// Search for the aliment from the database, and if found, add it to the journal
				found, err := afdw.FindAlimentByName(o.addName)
				if err != nil {
					return err
				}
				if err := afdw.AddAlimentToJournal(found, o.addDate); err != nil {
					return err
				}
				fmt.Printf("Added %v to %v journal!\n", found, o.addDate)

			} else {
				alimentFromCli := a.Aliment{Name: o.addName, Calories: o.addCal, Proteins: o.addProt}
				// Add the aliment to the database if --save (-s) is passed
				if o.save {
					_, err := afdw.FindAlimentByName(o.addName)
					if err == nil {
						return errors.New("We already have this item in the database. Can't save!")
					} else {
						// "Not found" error is expected: it means we're actually creating a new entry
						// Only check for other error types
						notFound := &io.NotFoundError{}
						if err != nil && !errors.As(err, notFound) {
							return err
						}
					}

					if err := afdw.AddAlimentToDatabase(alimentFromCli); err != nil {
						return err
					}
					fmt.Printf("Added %v to the database!\n", alimentFromCli)
				}
				// Add the aliment to the journal
				if err := afdw.AddAlimentToJournal(alimentFromCli, o.addDate); err != nil {
					return err
				}
				fmt.Printf("Added %v to %v journal!\n", alimentFromCli, o.addDate)
			}
			return nil
		},
	}

	addCmd.Flags().StringVarP(&o.addDate, "date", "d", time.Now().Format("2006-01-02"), "Add a journal entry for this date")
	addCmd.Flags().StringVarP(&o.addName, "name", "n", o.addName, "Name of the food consumed. If used with '-raw' and '-save' this will become the name of the new aliment entry")
	addCmd.Flags().BoolVarP(&o.save, "save", "s", o.save, "Whether to create a database entry with this data or not")
	addCmd.Flags().BoolVarP(&o.raw, "raw", "r", o.raw, "Whether to create a database entry with this data or not")
	addCmd.Flags().Float64VarP(&o.addCal, "calories", "c", o.addCal, "Amount of calories, in Kcal, for this entry")
	addCmd.Flags().Float64VarP(&o.addProt, "proteins", "p", o.addProt, "Amount of proteins, in grams, for this entry")

	addCmd.MarkFlagRequired("name")
	addCmd.MarkFlagsRequiredTogether("calories", "proteins", "raw")
	return addCmd
}
