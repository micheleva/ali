*********
Ali
*********

About the project
=================

Ali is an application to track daily nutrition intake


License
=======

GNU General Public License v3.0

See `COPYING <COPYING>`_ to see the full text.

