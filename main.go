// Copyright: (c) 2023, Michele Valsecchi <https://gitlab.com/micheleva>
// GNU General Public License v3.0+
// (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

package main

import (
	"fmt"
	"os"

	"gitlab.com/micheleva/ali/io"
	"gitlab.com/micheleva/ali/root"
)

func main() {
	if err := io.DbInit(); err != nil {
		fmt.Printf("Error initializing the database. We've got the following error: %v. Abort.\n", err)
		os.Exit(1)
	}

	rootCmd := root.NewCmdRoot()
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
	os.Exit(0)

}
