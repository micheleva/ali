// Copyright: (c) 2023, Michele Valsecchi <https://gitlab.com/micheleva>
// GNU General Public License v3.0+
// (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

// Package io handles the read and write bits of ali
package io

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"path"
	"strings"

	a "gitlab.com/micheleva/ali/aliments"
)

const (
	dbRoot       string = "database"
	journalsRoot string = "database/journals"
	alimentsRoot string = "database/aliments"
	dbFullPath   string = "database/aliments/db.json"
)

type NotFoundError struct{}

func (e NotFoundError) Error() string {
	return "No such item in the db"
}

// GetCorrectPath returns a string with the absolute path for targetPath if the program is executed as compiled binary.
// Othwerwise returns the relative path to targetPath
func GetCorrectPath(targetPath string) (string, error) {
	binaryPath, err := os.Executable()
	if err != nil {
		panic(fmt.Sprintf("Unable to get the path of the executable, error: %v . Abort.", err))
	}
	var correctPath string = ""
	switch targetPath {
	case "dbRoot":
		correctPath = dbRoot
	case "journalsRoot":
		correctPath = journalsRoot
	case "alimentsRoot":
		correctPath = alimentsRoot
	case "dbFullPath":
		correctPath = dbFullPath
	default:
		return "", errors.New("Unknown key: the database structure has no such path!")

	}

	if strings.HasPrefix(binaryPath, "/tmp/go-build") {
		// Case in which we're calling the program from `go run .` ... or Case in which the user has moved the binary to /tmp/go-build for any weird reason :-/
		return correctPath, nil
	} else {
		// Case in which we're calling the program for a compiled binary
		return path.Dir(binaryPath) + "/" + correctPath, nil
	}
}

type DbInitializer interface {
	DbInit() error
}

type DefaultDbInitializer struct{}

func (dbi *DefaultDbInitializer) DbInit() error {
	return dbi.DbInit()
}

type DatabaseHelper interface {
	GetCorrectPath(string) (string, error)
}

type DefaultDatabaseHelper struct{}

func (dbi DefaultDatabaseHelper) GetCorrectPath(path string) (string, error) {
	return GetCorrectPath(path)
}

// DbInit creates the folders structure to hold the data
func DbInit() error {
	//NICE TO HAVE: check for symlinks, as os.Executable() will not follow symlinks automatically
	dbRoot, err := GetCorrectPath("dbRoot")
	if err != nil {
		panic(err)
	}
	if err := os.Mkdir(dbRoot, os.ModePerm); err != nil && !errors.Is(err, os.ErrExist) {
		return err
	}
	journalsRoot, err := GetCorrectPath("journalsRoot")
	if err != nil {
		panic(err)
	}
	if err := os.Mkdir(journalsRoot, os.ModePerm); err != nil && !errors.Is(err, os.ErrExist) {
		return err
	}
	alimentsRoot, err := GetCorrectPath("alimentsRoot")
	if err != nil {
		panic(err)
	}
	if err := os.Mkdir(alimentsRoot, os.ModePerm); err != nil && !errors.Is(err, os.ErrExist) {
		return err
	}

	var syntaxError *json.SyntaxError
	dbFullPath, err := GetCorrectPath("dbFullPath")
	if err != nil {
		panic(err)
	}
	if _, err := ReadAlimentArrayFromFile(dbFullPath); err != nil {
		if errors.Is(err, os.ErrNotExist) {
			f, err := os.Create(dbFullPath)
			defer f.Close()
			if err != nil {
				return err
			}
			// Initialize the file with an empty array to avoid JSON syntax errors when reading this empty file
			if _, err := f.WriteString("[]"); err != nil {
				return err
			}
		} else if errors.As(err, &syntaxError) {
			fmt.Printf("The database file %v at is corrupted (malformed Json). Manual intervention is required!\n", dbFullPath)
			return err
		} else {
			// Permission error or anything other one
			return err
		}
	}
	return nil
}

// ReadAlimentArrayFromFile returns Aliments stored in given json file
func ReadAlimentArrayFromFile(path string) ([]a.Aliment, error) {
	file, ioErr := os.ReadFile(path)
	if ioErr != nil {
		return []a.Aliment{}, ioErr
	}
	data := []a.Aliment{}
	err := json.Unmarshal(file, &data)
	if err != nil {
		return nil, err
	}
	return data, nil
}

// FindAlimentByName returns an Aliment from the database (a json file) using its name as key
func FindAlimentByName(alimentName string) (a.Aliment, error) {
	dbFullPath, err := GetCorrectPath("dbFullPath")
	if err != nil {
		return a.Aliment{}, err
	}
	foundAls, err := ReadAlimentArrayFromFile(dbFullPath)
	if err != nil {
		return a.Aliment{}, err
	}
	for _, item := range foundAls {
		if item.Name == alimentName {
			return item, nil
		}
	}
	return a.Aliment{}, NotFoundError{}
}

// AddAlimentToDatabase adds an Aliment to the database (a json file)
func AddAlimentToDatabase(newEntry a.Aliment) error {
	dbFullPath, err := GetCorrectPath("dbFullPath")
	if err != nil {
		return err
	}
	arrayOfAliments, err := ReadAlimentArrayFromFile(dbFullPath)
	if err != nil {
		return err
	} else {
		arrayOfAliments = append(arrayOfAliments, newEntry)
		if err := writeAlimentsToFile(arrayOfAliments, dbFullPath); err != nil {
			return err
		}
	}
	return err
}

// AddAlimentToJournal records an Aliment consumption for a given date
func AddAlimentToJournal(newEntry a.Aliment, date string) error {
	var syntaxError *json.SyntaxError
	journalsRoot, err := GetCorrectPath("journalsRoot")
	if err != nil {
		return err
	}

	dirPath := fmt.Sprintf(journalsRoot+"/%v", date)
	filePath := fmt.Sprintf(journalsRoot+"/%v/data.json", date)
	arrayOfAliments, err := ReadAlimentArrayFromFile(filePath)
	// We need to check multiple errors, as the init() called in main.go only checks for well formed DbFullPath,
	// but not for each dairy file
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			createErr := os.Mkdir(dirPath, os.ModePerm)
			if createErr != nil && !errors.Is(createErr, os.ErrExist) {
				return createErr
			}
			_, fileErr := os.Create(filePath)
			if fileErr != nil {
				return fileErr
			}
		} else if errors.As(err, &syntaxError) {
			fmt.Printf("It seems the journal file for %v is not wellformed JSON\n", date)
			return err
		} else {
			fmt.Println("We had a generic error:", err)
			return err
		}
	}
	if err == nil {
		// Case in which this journal file has already one entry
		arrayOfAliments = append(arrayOfAliments, newEntry)
		err = writeAlimentsToFile(arrayOfAliments, filePath)
	} else {
		// Case in which this is the first write to this empty file
		var newArray []a.Aliment
		newArray = append(newArray, newEntry)
		err = writeAlimentsToFile(newArray, filePath)
	}
	return err
}

func writeAlimentsToFile(data []a.Aliment, path string) error {
	f, err := os.Create(path)
	if err != nil {
		return err
	}
	jsonAliment, jsonErr := json.Marshal(data)
	if jsonErr != nil {
		return jsonErr
	}
	_, wErr := f.WriteString(string(jsonAliment))
	if wErr != nil {
		return wErr
	}
	syncErr := f.Sync()
	if syncErr != nil {
		return syncErr
	}
	defer f.Close()
	return nil
}
